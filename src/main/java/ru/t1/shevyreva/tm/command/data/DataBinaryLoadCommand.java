package ru.t1.shevyreva.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.Domain;
import ru.t1.shevyreva.tm.enumerated.Role;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class DataBinaryLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-bin";
    @NotNull
    private final String DESCRIPTION = "Load Data from binary file.";

    @Override
    public @Nullable String getName() {
        return NAME;
    }

    @Override
    public @Nullable String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @SneakyThrows
    @Override
    public @Nullable void execute() {
        System.out.println("[DATA BINARY LOAD]");
        @NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_BINARY);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        objectInputStream.close();
        fileInputStream.close();
        setDomain(domain);
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
